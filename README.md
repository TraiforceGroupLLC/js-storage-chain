## JS Storage Chain
JS Storage Chain is a modular library set to support IPFS file storage and distrubution.

JS Chains purpose is simple to store all supported javascript projects in a modular manner that is similar to monorepo but can provide a root source for packaged deployment and local development.

### Methodology
 
The method is to decouple basic javascript and typescript package/project/module management. Frameworks offer opinionated implementations to needs of applications/CLI/services. Javascript has evolved to the point where it can run the browser, be optimized, assembled into webassembly and apply the common best practices. The purpose of this library is my attempt to specialize in javascript technologies and iterate with many stacks in one location. Yet allowing all modules to be exported to npm repositories independently.
 
I have chosen to utilize only ES modules, and leave the webpacking and such to applications or libraries use cases. ES6 should suffice for optimization, and clear encapsulation boundaries. This being said libraries dependencies are not expected to support ES6 modules but it is a preference.
 
All javascript projects for Traiforce Group LLC’s goals should exist here, until the operational framework is factored out. For now the project structure, and requirements will be documented via Git, and dependencies will be called out in each readme. Eventually I would like to make a docs system to be able to discuss why each dependency is needed and has value, either to the community and imported for maintenance, or for Traiforce Group LLC products.
 
This repo is for modularized javascript code related to traiforce group llc’s interests. (aka Stephen Traiforos’s interests that will later become a team that will need to incorporate collaboration strategies. Like Git issues, or a mixture of slack, and issues etc.)
 
All cloud services should be well understood and a backup local environment should at the very least needs to be proposed. This project should be modular even in application strategy. Say a web application uses a AWS Scalable database, there should be a strategy for local instances of a given environment and should if requiring resources have a test api/mock api. Where chains are being used, say git, or ipfs, orbit db feeds there should be a close to production cut to ensure developer access to reproduction of reported issues. Example: think of an application bug reporting tool able to isolate a User Experience chain (flux event queue stored using middleware), Resource chain (orbitDB), and a timestamp range to query other databases for information. This would allow for more fringe scenarios to be isolated at an enterprise management level. Our tools should make everything easier. As developers we are constantly asked to do more, and our industry pushes back. Yet this specialization creates a large ability for siloing and not allowing cross pollination. The goal of this project is to prove the insane idea I propose. To create a storage platform that supports extension and open use. Adding constantly to compete with google drive, dropbox, Box etc. Yet allowing true isolation of data ownership, no more egregious ToS, plain english with the idea of supporting technologies that have the capability of making our lives easier, more ethical. 
 
Blobchain:
 - Planetary
    - Mercury
    - Venus
    - Earth
    - Mars
    - ...
- Galaxy
    - Data is no longer the planets but the sun
    - Milkyway
- InterGalactic 
    - Something cool.
 
Community Stream will be the next evolution of data storage and distribution with security in place yet in a distribution that is collapse tolerant. Imagine YouTube Servers going poof, nope! I want a data chain that we can have nodes hold chunks so if my web service or communitystream.io is down a node runs the scripts here or in another designated fork/repo in another language to rehost the web application and resolve the data chains for resources. Storage Chain is the base case and is inspiring. Working on Open Collectives to try and make a transparent organization from the beginning. 
