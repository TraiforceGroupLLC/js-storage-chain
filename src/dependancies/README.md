The purpose of this document is to be a placeholder for additonal information the dependancy folder requires for extension or modification. 

Dependancies folder is for external libraries like ipfs, etc. JS libraries move fast and having the source available for developers to see is important. It will allow greater innovation and remerging changes in existing libraries proving where requirements forced divergence. Yet holding to supporting these projects.

## Tutorial:
    - Adding libraries
    - modification of libraries for roadblocks.
        - migidation to incorparting learnings with Open Source libraries.
        - Determine if points of migration can be answered.
    - removing a library
    - updating a library
    - creating a library DOES NOT belong here.
        - That lives in this libraries src/libraries for simplicity. If it depends on something unique say polyglot methodology it should be imported as an external dependancy and the stack may need to be reassessed. Focusing on JS/TS and its entire community.