## Libraries repo
All librairies should run standalone, if using cloud technologies it should be named as such and external additional libraries should be abstracted out for global use. For example:
Configuration using AWS paramater store versus MS Azure secure variable storage etc etc. 

Twelve factor app compliant configuration library;
    - AWS Paramater Store SDK adapter for configuration library interfaces.
    - Azure secure parmeters
    - Google cloud
    - Local secure configuration for local use that a end user could utilize not a developer insecure junk.

Yet those scenarios would be nice to utilize a builder pattern approach the can plug into existing modules that can support rapid application and service development. The point remains always rebundle work into new libraries to retain seperation of concern. Other organizations have tackled this problem using NRWL and other tools like lerna but I think yarn workspaces may be the way top build a holistic approach that uses existing functionality without libraries abstracting everything away with little clarity how that is accomplished.

ES 6 module creation - windows based will be abstracted to node tools to ensure interoperability.

    ```
    // Generate source directories for project in order.
        mkdir {library-name}
        cd {library-name}
        mkdir src
        mkdir src/interfaces
        mkdir src/constants

        // package management intialization
        yarn config set -- set all settings so we do not need to have issues for dev onboarding....
        yarn init 
        touch README.md

    ```