import IPFSAWSS3 from "../src";
import { expect } from 'chai';

const ipfsPath = '/blobchain/ipfs/secure';
const ipfsAwsS3Bucket = 'block-stream-ipfs-store';
let node: any = null;

after(async function(){
    if(node){
         await node.stop();
    }
    return new Promise(()=>console.log("node was not set on destruction"));   
 });
describe("IPFS should", function() {
    it("Start and not stop unless process is killed:  creates leak and locked repo", async function(){
        async function connectOrCreateWithAssumedCredentials() {
            const ipfs: Promise<any> = new IPFSAWSS3()
                    .createOrConnect(ipfsPath, ipfsAwsS3Bucket);
            node = await ipfs;
            expect(node).to.have.own.property('start');
            await node.start();
            return node;
    }
    });

    it("start and ping private node specified", async function(){
        async function connectOrCreateAndPingExistingPeer() {

            const ipfs: Promise<any> = new IPFSAWSS3()
                    .createOrConnect(ipfsPath, ipfsAwsS3Bucket);
            const node = await ipfs;
            expect(node).to.have.own.property('start');
            await node.start();
            expect(node).to.have.own.property('ping');
            return node;
        }
    });

    it("start and pin private CID", async function(){
        async function connectOrCreateAndPingExistingPeer() {

            const ipfs: Promise<any> = new IPFSAWSS3()
                    .createOrConnect(ipfsPath, ipfsAwsS3Bucket);
            node = await ipfs;
            expect(node).to.have.own.property('start');
            await node.start();
            expect(node).to.have.own.property('ping');
            return node;
    }
    });
}
)