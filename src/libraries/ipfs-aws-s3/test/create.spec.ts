import IPFSAWSS3 from '../src';
import {expect} from 'chai';

const ipfsPath = '/blobchain/ipfs/secure';
const ipfsAwsS3Bucket = 'block-stream-ipfs-store';
let node: any = null;

after(async function(){
   if(node){
        await node.stop();
   }
   return new Promise(()=>console.log("node was not set on destruction"));   
});

describe('creates aws node with credentials on local machine', () =>
    it("should create or connect", 
        async function connectOrCreateWithAssumedCredentials() {
                const ipfs: Promise<any> = new IPFSAWSS3()
                        .createOrConnect(ipfsPath, ipfsAwsS3Bucket);
                node = await ipfs;
                expect(node).to.have.own.property('start');
                //console.log("start checked",node);
                //const startResults = await node.start();
                //console.log("started",startResults);
                console.log("pining",node.pin);
                const pinResults = await node
                        .pin("QmQaskazEgVyb7afzwsqbodvHz2DzfSnCXupARjv5d8arP");
                console.log("started",pinResults);
                return node;
        }
)      
);

