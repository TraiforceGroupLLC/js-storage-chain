export interface IIPFSAWSS3 {
    createOrConnect(path:string, awsS3Bucket: string): Promise<any>;
    createMultiAddress(ip: string, peerIdHash: string, ipv6: boolean): string;
};
