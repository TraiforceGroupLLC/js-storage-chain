import { IIPFSAWSS3 } from "./interfaces/ipfs-aws-s3.interface";

export class IPFSAWSS3 implements IIPFSAWSS3 {

    async createOrConnect(path:string, bucket: string): Promise<any> {
        const IPFS = require('ipfs');
        const { createRepo } = require('datastore-s3');
        const repo = createRepo({path}, {bucket});
        const ipfs = IPFS.create({repo});
        return ipfs;
    }

    createMultiAddress(ip: string, peerIdHash: string, ipv6: boolean = true): string {
        let address = null;
        if(ipv6){
          address = "/ip6/";
        } else {
          address = "/ip4/";
        }
        address = address + ip + '/';
        address = address + "/tcp/4001/ipfs/";
        address = peerIdHash;
        return address
    } 
};

export default class Main {
  async main(args:any[]){
    const ipfsPath = '/blobchain/ipfs/secure';
    const ipfsAwsS3Bucket = 'block-stream-ipfs-store';
    const ipfs: Promise<any> = new IPFSAWSS3()
    .createOrConnect(ipfsPath, ipfsAwsS3Bucket);
    let node = await ipfs;
    if(node.hasOwnProperty("start")){
      console.log("pining",node.pin);
      const pinResults = await node
          .pin.add("QmQaskazEgVyb7afzwsqbodvHz2DzfSnCXupARjv5d8arP");
    }
    //console.log("start checked",node);
    //const startResults = await node.start();
    //console.log("started",startResults);
  }

  mainRun(){
    this.main([])
    .then(()=> console.log("success"))
    .catch(console.error);
  }

  async createOrConnect(path:string, bucket: string){
    return await new IPFSAWSS3().createOrConnect(path, bucket);
  }
}

const runApplication = () => new Main().mainRun();
runApplication();