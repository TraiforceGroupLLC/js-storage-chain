## IPFS Secured CDN Private Network

### Why?

IPFS - Interplanetary file system is a innovate way to store static files for public access (no exceptions here!), which can provide free commercial and noncommercial media hosting. Such as static webapplications and so much more.

BUT it comes with an attack vector at it's insecure http provider. Truthfully I trust the peers ability to check datas hash before accepting it. Yet I can not allow my peers to have an insecure vector.

This project is to decouple the OSI Transport Layer Encryption from the plain text HTTP - Hyper Text Transfer Protcol. I want to make a solution to CA certificate authorities in Distributed web compatatible methods for SSL - Secure Socket Layers over http. For now we will use containers on open shift to orchestrate the requirements we have to join a https proxy server instance from the http enabled IPFS peer.

## Open Shift Development

Open Shift by Red Hat is a container cluster management platform to accelerate development and pipeline scalability concerns. Providing Ingress, and Egress capabilties that allow for security to be organization wide and not per service.